<?php
include "components/allActionOnblog.php";
$blogs = queryblog("SELECT * FROM postingan");


?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Programming Blog</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <?php require "components/navbar.php"?>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>WELCOME</h1>
            <span class="subheading">A Blog Theme by Start to learn</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container-fluid" id="postingan">
    <div class="row">
      <div class="col-lg-8 col-md-10 col-12 mx-auto">
        <div class="row">
        <?php foreach($blogs as $blog_list):?>
        <div class="post-preview col-md-6 col-lg-4 text-center">
          <a href="post.php?id=<?=$blog_list['id'];?>">
            <?php if($blog_list['foto_thumbnail']==null):?>
              <div class="d-flex justify-content-center">
            <img src="https://lh3.googleusercontent.com/proxy/gHW5jqaHRNMx2sPOGPi_Gdn5TzddtvXBGljYcdNZ4Bgisxs1LRRkTCZqVXgq8GuBSh3w9VHVquuII2qKCRFwhv6z09JfuRS2TMNSI12ZAFU7JwXdVyWk" style="width: 10em;" alt="">
          </div>
          <?php else:?>
          <div class="d-flex justify-content-center">
            <img src="<?=$blog_list['foto_thumbnail'];?>" style="width: 10em;" alt="">
          </div>
          <?php endif;?>
            <h4 class="mt-3">
            <?=$blog_list['judul_postingan_blog'];?>
            </h4>
            <h6 class="post-subtitle mt-3">
            kategori : <?=$blog_list['kategori'];?>
            </h6>
          </a>
          <p class="post-meta" style="font-size: 0.7em;">Posted by
            <a href="#"><?= $blog_list['author']?></a>
            on <?php date_default_timezone_set('Asia/Jakarta');
                                                     echo date('d-F-Y',strtotime($blog_list['tanggal_publish'])); ?></p>
        </div>
        <hr>
        <?php endforeach;?>
      </div>
        <!-- Pager -->
        <div class="clearfix">
          <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2021</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
