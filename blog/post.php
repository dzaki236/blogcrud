<?php
require "../blog/components/allActionOnblog.php";
$id_post = $_GET['id'];
$article = queryblog("SELECT * FROM postingan WHERE id='$id_post'")[0];
if(!isset($id_post)){
  header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <?php require "components/navbar.php" ?>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>WELCOME</h1>
            <span class="subheading">A Blog Theme by Start to learn</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <?= $article['isi_postingan_blog']; ?>
          <p>Creator by
            <a href="#"><?= $article['author']; ?></a>. Photographs by internet</a>.
          </p>
        </div>
      </div>
      <!-- Komentar -->
      <!-- Komentar -->
      <?php $post_komen = queryblog("SELECT * FROM komentar WHERE id_postingan = $id_post"); ?>
<?php 
if (isset($_POST["komentar"])) {

// cek element tambah data dan methodnya
if (komentar($_POST) > 0) {
    echo "<script>"."alert('Refresh kembali untuk melihat komentar mu');"."</script>";
    echo "<script>"."document.location.href=post.php?id='$id_post';"."</script>"; // > 0 == berhasil
} else {
  echo "<script>"."alert('Komentar gagal di tambahkan,cek kembali');"."</script>";
  echo "<script>"."document.location.href=post.php?id='$id_post';"."</script>";
}
}?>
      <form action=" " method="POST">
        <h1 class="text-center">Komentar</h1>
        <p class="text-center">Berikan komentar yang membangun :)</p>
        <div class="form-group">
          <input type="hidden" class="form-control" id="exampleFormControlInput1" name="id_postingan" value="<?=$id_post;?>" placeholder="name@example.com">
        </div>
        <div class="form-group">
          <label for="exampleFormControlInput1">Email</label>
          <input type="email" class="form-control" id="exampleFormControlInput1" name="email_komentar" placeholder="name@example.com">
        </div>
        <br>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Berikan tanggapan mu terkait blog ini</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" name="isi_komentar" rows="3"></textarea>
        </div>
        <div class="form-group">
        <button type="submit" name="komentar" value="nokomen" class="btn btn-outline-secondary">Kirim Komentarmu</button> <span class="text-secondary font-italic" style="font-size: 1em;"> ingat,komentar tidak dapat di hapus kembali,jadi bijaklah dalam berkomentar :)</span>
        </div>
      </form>

      <br>
      <hr>
      <br>
        
      <?php foreach ($post_komen as $komentar) : ?>
        <div class="card mt-3">
          <div class="card-body">
            <h6 class="card-subtitle mb-2 text-muted"><?= $komentar['email']; ?></h6>
            <p class="card-text"><q> <?= $komentar['isi_komentar']; ?></q></p>
            <a href="#" class="card-link"><?= date('d-F-Y', strtotime($komentar['tanggal_komentar'])); ?></a>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </article>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2020</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
 
</body>

</html>