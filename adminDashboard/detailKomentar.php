<?php
// session if user login
session_start();
if(!isset($_SESSION['login'])){
    header('Location: login.php');
    exit;
}
include "./components/allAction.php";
$id_post = $_GET['id'];
$article = query("SELECT * FROM postingan WHERE id='$id_post'")[0];
if(!isset($id_post)){
    header('Location: komentar.php');
}

?>
<?php $komen = query("SELECT * FROM komentar WHERE id_postingan = $id_post");?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>

<body>

    <div class="wrapper">
        <?php include "./components/sidebar.php" ?>

        <div class="main-panel">
            <?php include "./components/navbar.php" ?>


            <div class="content">
                <div class="container-fluid">
                    <div class="row"> <center><h2><?= $article['judul_postingan_blog'] ?></h2></center>
                    <br>
                        <div class="card" style="padding: 1em;">
                            <div>
                               
                                <div><?= $article['isi_postingan_blog']; ?></div>
                                <div><?= date('d-F-Y H:i:s',strtotime($article['tanggal_publish'])); ?></div>
                            </div>
                        </div>
                        <center><h1><?= "total komentar : ".count($komen);?></h1></center>
                        <?php foreach($komen as $komentar):?>
                        <div class="card" style="padding:1em;">
                        <h4 class="text-info"><?= $komentar['email']; ?></h4>
                        <h5><q><i><?= $komentar['isi_komentar']; ?></i></q></h5>
                        <p><?= date('d-F-Y H:i:s',strtotime($komentar['tanggal_komentar'])); ?></p><a href="hapusKomentar.php?id=<?=$komentar['id'];?>" class="btn btn-danger btn-block"><b>Hapus Komentar</b></a>
                        </div>
                        
                        <?php endforeach;?>
                    </div>
                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>

                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright pull-right">
                        &copy; <script>
                            document.write(new Date().getFullYear())
                        </script>, made with <i class="fa fa-heart heart"></i> by <a href="#">Dzaki Ahnaf Z</a>
                    </div>
                </div>
            </footer>

        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="assets/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>
</body>

</html>