<?php
// session if user login
session_start();
if(!isset($_SESSION['login'])){
    header('Location: login.php');
    exit;
}
require "./components/allAction.php";

// Cek apakah eror atau tidak
$users = query("SELECT * FROM admins WHERE nama_admin <> 'admin'");

// Ambil data postingan dari object nya(fetch)

// var_dump($postingan["judul_postingan_blog"]);
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>

<body>

    <div class="wrapper">
        <?php include "./components/sidebar.php" ?>

        <div class="main-panel">
            <?php include "./components/navbar.php" ?>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="header">

                                    <h4 class="title">User disini</h4>

                                    <form>
                                       
                                            
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label><a style="margin-top:1em;" href="./tambahDatauser.php" class="btn mt-5 btn-info btn-fill btn-sm">[+] Tambah User</a></label>
                                                </div>
                                            </div>
                                        <div class="clearfix"></div>
                                    

                                </div>
                                <div class="content table-responsive table-full-width">
                                    <table class="table table-striped w-100">
                                        <thead>
                                            <th class="text-warning">Nomor</th>
                                            <th class="text-warning"><b>Nama</b></th>
                                            <th class="text-warning"><b>Tentang admin</b></th>
                                            <th class="text-warning"><b>Nomor Hp</b></th>
                                            <th class="text-warning"><b>Action</b></th>
                                        </thead>
                                        <tbody>
                                            <?php $nomor = 1;
                                            foreach ($users as $user_list) : ?>
                                                <tr>
                                                    <td><?= $nomor++; ?></td>
                                                    <td><?= $user_list['nama_admin']; ?></td>
                                                    <td><?php echo $user_list['about_user']; ?></td>
                                                    <td><?= $user_list['nomor_hp']; ?></td>
                                                    <td class="text-center"><button onclick='konfirmasi(event,"<?= $user_list["nama_admin"];  ?>")' class="btn btn-danger btn-fill font-weight-bolder ">Delete</button>
                                                        <hr><a href="editUser.php?nama_admin=<?=$user_list['nama_admin']; ?>" class="btn btn-warning btn-fill font-weight-bolder">Edit</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>

                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright pull-right">
                        &copy; <script>
                            document.write(new Date().getFullYear())
                        </script>, made with <i class="fa fa-heart heart"></i> by <a href="#">Dzaki Ahnaf Z</a>
                    </div>
                </div>
            </footer>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.5/dist/sweetalert2.all.min.js"></script>
    <script>
        function konfirmasi(e, nama_admin) {
            Swal.fire({
                title: 'Do you want to save the changes?',
                showDenyButton: true,
                confirmButtonText: `Save`,
                denyButtonText: `Don't save`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    var http = new XMLHttpRequest(); // inisialitation
                        http.open("GET", "hapusUser.php?nama_admin=" + nama_admin, true); // tentukan server 
                        http.send(); // eksekusi
                        e.returnValue = true;
                    Swal.fire('Saved!', '', 'success');
                    setTimeout(function() {
                            location.reload("user.php");
                        }, 1000);
                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        }
    </script>
<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="assets/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>


</html>

</body>
