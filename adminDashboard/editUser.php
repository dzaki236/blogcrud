<?php
// session if user login
session_start();
if(!isset($_SESSION['login'])){
    header('Location: login.php');
    exit;
}
require "./components/allAction.php";
$req = $_GET['nama_admin'];
$editusers = query("SELECT * FROM admins WHERE nama_admin='$req'")[0];
echo '
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.6/dist/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.6/dist/sweetalert2.all.min.js"></script>
';
if (isset($_POST["edit_user"])) {
    var_dump($_POST);
    // cek element tambah data dan methodnya
    if (editdatauser($_POST) > 0) {
        
        echo '<script>alert("data berhasil di tambahkan");
        document.location.href="postingan.php";</script>'; // > 0 == berhasil
        
    } else {
        //     echo '<div class="alert container alert-danger alert-dismissible fade show" role="alert">
        //     <strong>Maaf</strong> Data gagal di ubah
        //     <button type="button" class="btn" data-bs-dismiss="alert" aria-label="Close"><i class="text-danger ml-5 mdi mdi-close h1" onclick="link()"></i></button>
        //   </div>';
    }
}

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Sweet allert -->

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>

<body>

    <div class="wrapper">
        <?php include "./components/sidebar.php" ?>

        <div class="main-panel">
            <?php include "./components/navbar.php" ?>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <form action=" " method="POST">

                            <div class="row">
                                <div class="col-md-12">
                                <div class="form-group">
                                        <label>Nama (tak dapat dirubah)</label>
                                        <input type="hidden" name="id_admin" value="<?= $_GET['nama_admin'];?>">
                                        <input type="text"  name="nama_admin" value="<?= $editusers['nama_admin']; ?>" class="form-control border-input" placeholder="" >
                                    </div>

                                    <div class="form-group">
                                        <label>Nomor Hp</label>
                                        <input type="number" name="nomor_hp" value="<?= $editusers['nomor_hp']; ?>" class="form-control border-input" placeholder="Tulis no Hp " required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Tulis Bio</label>
                                        <textarea name="about_user" id="tiny"><?= $editusers['about_user']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="edit_user" value="edit" class="btn btn-block btn-info btn-fill btn-wd">Edit User</button>

                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>

                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright pull-right">
                        &copy; <script>
                            document.write(new Date().getFullYear())
                        </script>, made with <i class="fa fa-heart heart"></i> by <a href="#">Dzaki Ahnaf Z</a>
                    </div>
                </div>
            </footer>

        </div>
    </div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/jquery.tinymce.min.js" referrerpolicy="origin"></script>
<script>
    $('textarea#tiny').tinymce({
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount image code'
        ],
        toolbar: 'undo redo | formatselect | bold italic backcolor | link image | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
    });
</script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="assets/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>