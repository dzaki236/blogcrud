<?php
include "./components/allAction.php";
$postingan_list = query("SELECT * FROM postingan ");

$html='
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Generate HTML to PDF</title>
	<style>
	*{text-align:center;}
	</style>
</head>
<body>	
	<h1>Generate HTML to PDF</h1>
	<table border="1" cellpadding="10" cellspacing="0" width="100%">
		<tr>
			<th>No</th>
			<th>Judul Postingan</th>
			<th>Kategori</th>
            <th>Tanggal Publish</th>
		</tr>';
		$i = 1; 
        foreach($postingan_list as $postingan){
            $html .= '<tr>
            <td>'.$i++.'</td>
            <td>'.$postingan['judul_postingan'].'</td>
            <td>'.$postingan['kategori'].'</td>
            <td>'.date('d-F-Y H:i:s',strtotime($postingan['tanggal_publish'])).'</td>
            </tr>';
        }
	$html .= '</table>	
</body>
</html>';

require './vendor/autoload.php';
use Dompdf\Dompdf;

$dompdf= new Dompdf();

$dompdf->loadHtml($html);

$dompdf->setPaper('A4','portrait');

$dompdf->render();

$dompdf->stream("playerofcode",array("Attachment"=>0));


// Ambil data postingan dari object nya(fetch)

// var_dump($postingan["judul_postingan_blog"]);
?>
