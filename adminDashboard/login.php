<?php
// session if user login
session_start();
// if(!isset($_SESSION['login'])){
//     header('Location: login.php');
//     exit;
// }
// include all function

if(isset($_SESSION['login'])&&$_SESSION['login']){
    header('Location: dashboard.php');
}
include "./components/allAction.php";
$_SESSION['login']=false;
if (isset($_POST["login"])) {
    
    // login logic
    $username = $_POST["username"];
    $password = $_POST["password"];
    // ngecek login
    $hasils = mysqli_query($koneksi, "SELECT * FROM admins WHERE nama_admin = '$username'");

    // cek username
    if (mysqli_num_rows($hasils) === 1) {
        // cek password
        $users = mysqli_fetch_assoc($hasils);
        if (password_verify($password, $users["password"])) {
            echo "<script>alert('username berhasil tedaftar');
            </script>";
            // set session,jika login
            $_SESSION["user"] = $username;
            $_SESSION["login"] = true;
            header("Location: dashboard.php");
            exit;
        }
    }
    $eror = true;
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>

<body>

    <div class="wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row" style="margin: 2em;">
                    <form action="" method="POST">
                        <center>
                            <h1>LOGIN</h1>
                            <label for="">Login sebagai admin</label>
                        </center>
                        <?php if (isset($eror)) : ?>
                            <p class="text-danger">Username/Password salah</p>
                        <?php endif; ?>
                        <div class="card">

                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" value="" name="username" placeholder="Username" class="form-control border-input" />

                            </div>
                            <br>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" value="" name="password" placeholder="Password" class="form-control border-input" />

                            </div>
                            <br>
                            <div class="form-group">    
                                <button type="submit" name="login" value="login" class="btn btn-default btn-block">Login</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>
                        document.write(new Date().getFullYear())
                    </script>, made with <i class="fa fa-heart heart"></i> by <a href="#">Dzaki Ahnaf Z</a>
                </div>
            </div>
        </footer>

    </div>
    </div>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="assets/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>
</body>

</html>