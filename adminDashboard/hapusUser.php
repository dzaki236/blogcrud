<?php 
// session if user login
session_start();
if(!isset($_SESSION['login'])){
    header('Location: login.php');
    exit;
}
require "./components/allAction.php";
$name = $_GET["nama_admin"];

// cek element tambah data dan methodnya
if (hapususer($name) > 0) {
    echo '<script>document.location.href="postingan.php";</script>'; // > 0 == berhasil
} else {
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Maaf</strong> Data gagal di tambahkan
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>';
}
?>