<div class="sidebar sidebar-offcanvas" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    Creative Blog
                </a>
            </div>

            <ul class="nav">
                <br>
                <li class="nav-item menu-items m-3">
                    <a class="nav-link" href="dashboard.php">
                        <i class="ti-pie-chart"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <hr>
                <li class="nav-item menu-items mt-3">
                    <a class="nav-link" href="postingan.php">
                        <i class="ti-bookmark-alt"></i>
                        <p>Postingan</p>
                    </a>
                </li>
                <hr>
                <li class="nav-item menu-items mt-3">
                    <a class="nav-link" href="komentar.php">
                        <i class="ti-comment-alt"></i>
                        <p>Komentar</p>
                    </a>
                </li>
                <hr>
                <li class="nav-item menu-items mt-3">
                    <a class="nav-link" href="kategori.php">
                        <i class="ti-list"></i>
                        <p>Kategori</p>
                    </a>
                </li>
                <?php if($_SESSION['user']=='admin'):?>
                <hr>
                <li class="nav-item menu-items mt-3">
                    <a class="nav-link" href="user.php">
                        <i class="ti-user"></i>
                        <p>User</p>
                    </a>
                </li>
                <?php endif;?>
            </ul>
    	</div>
    </div>