<?php
// connection to database
$koneksi = mysqli_connect("localhost","root","abcd5dasar","blog");
// $results = mysqli_query($koneksi,"SELECT * FROM postingan");//2parameter ("koneksi","query")

function query($kueri){
    global $koneksi;
$hasil = mysqli_query($koneksi,$kueri);
echo $errno = !$hasil?mysqli_error($koneksi)." :)":null;
$rows = [];
while ($row = mysqli_fetch_assoc($hasil)){
    $rows []=$row;
}
return $rows;
}

function tambahdatapostingan($tambahdata){
    global $koneksi;
    $author = htmlspecialchars($tambahdata['author']);
    $judul = htmlspecialchars($tambahdata['judul_postingan_blog']);
    $thumbnail = htmlspecialchars($tambahdata['foto_thumbnail']);
    $isi_blog = $tambahdata['isi_postingan_blog'];
    $kategori = htmlspecialchars($tambahdata['kategori']);
    $insertquery = "INSERT INTO postingan (`author`,`judul_postingan_blog`,`foto_thumbnail`,`isi_postingan_blog`,`tanggal_publish`,`kategori`) VALUES ('$author','$judul','$thumbnail','$isi_blog',CURRENT_TIMESTAMP,'$kategori')";
    mysqli_query($koneksi, $insertquery);
    return mysqli_affected_rows($koneksi);
}
function editdatapostingan($editdata)
{global $koneksi;
    $id_postingan = $editdata["id"];
    $judul = htmlspecialchars($editdata['judul_postingan_blog']);
    $thumbnail = htmlspecialchars($editdata['foto_thumbnail']);
    $isi_blog = $editdata['isi_postingan_blog'];
    $kategori = htmlspecialchars($editdata['kategori']);
    $updatequery = "UPDATE postingan SET judul_postingan_blog = '$judul', foto_thumbnail = '$thumbnail', isi_postingan_blog='$isi_blog', kategori ='$kategori' WHERE id = $id_postingan";
    mysqli_query($koneksi, $updatequery);
    return mysqli_affected_rows($koneksi);
}

function tambahdatakategori($kategori){
    global $koneksi;
    $kategori = htmlspecialchars($kategori['kategori']);
    $insertquery = "INSERT INTO kategori (`kategori`) VALUES ('$kategori')";
    mysqli_query($koneksi, $insertquery);
    return mysqli_affected_rows($koneksi);
}
function hapuskategori($kategori){
    global $koneksi;
    mysqli_query($koneksi,"DELETE FROM kategori WHERE kategori = '$kategori'");
    return mysqli_affected_rows($koneksi);
    }

function hapuskomentar($id_komentar){
        global $koneksi;
        mysqli_query($koneksi,"DELETE FROM komentar WHERE id = '$id_komentar'");
        return mysqli_affected_rows($koneksi);
    }

    echo " <script>
if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}
</script>";

function hapuspostingan($ngepost){
    global $koneksi;
    mysqli_query($koneksi,"DELETE FROM postingan WHERE id = '$ngepost'");
    return mysqli_affected_rows($koneksi);
    }
function registrasi($user){
    global $koneksi;
    $username = strtolower(stripslashes($user['username']));
    $password = mysqli_real_escape_string($koneksi,$user['password']);
    $konfirm = mysqli_real_escape_string($koneksi,$user['konfirmasi_password']);
    // cek username
    $cek_double_users = mysqli_query($koneksi,"SELECT nama_admin FROM admins WHERE nama_admin='$username'");
    if(mysqli_fetch_assoc($cek_double_users)){
        echo "<script>alert('username telah terdaftar');</script>";
        return false;
    }
    // cek pass yang berbeda
    if($password !== $konfirm){
        echo "<script>alert('password gak sesuai');</script>";
        return false;
    }
    // encrypt password/pw acak,and algorithm acak
    $password = password_hash($password,PASSWORD_BCRYPT);
    // add username to database
    $regisquery = "INSERT INTO admins (`nama_admin`,`password`) VALUES ('$username','$password')";
    mysqli_query($koneksi,$regisquery);
    // return to function
    return mysqli_affected_rows($koneksi);
}

function hapususer($users){
    global $koneksi;
    mysqli_query($koneksi,"DELETE FROM admins WHERE nama_admin = '$users'");
    return mysqli_affected_rows($koneksi);
    }


    function editdatauser($editdatauser)
    {
        global $koneksi;
        $id_admin = $editdatauser["id_admin"];
        $nama = strtolower(htmlspecialchars($editdatauser["nama_admin"]));
        $no_hp = htmlspecialchars($editdatauser['nomor_hp']);
        $bio = $editdatauser['about_user'];
        $updatequery = "UPDATE admins SET nama_admin = '$nama', nomor_hp ='$no_hp', about_user = '$bio' WHERE nama_admin = '$id_admin'";
        echo $updatequery;
        mysqli_query($koneksi, $updatequery);
        return mysqli_affected_rows($koneksi);
    }
    